function G2M

scdkb=0.01;
dcdkb=0.01;

kdAS = 0.001;
kpmyb3 = 1;%0.9058;
kdpmyb3=0.25;%0.0317;
skp2a=0;
dskp2=0.01;

MYB3R3T=1;
sskp2=0.05;
%CDKBT=0.6;
ssmr=0.05;
dsmra=0.04;
dsmr=0.1;

kd=0.05;

CDKBT0=0;
SMR0=1;

y0=[CDKBT0];
tspan=500;
[ts,ys]=ode45(@F,[0 tspan],y0);

CDKBTs=ys(:,1);        
CDKBs=CDKBTs;
MYB3R3ps = MYB3R3T.*(kpmyb3).*CDKBs./((kpmyb3).*CDKBs + kdpmyb3);
MYB3R3s=MYB3R3T-MYB3R3ps;

figure(2)

subplot(2,1,1)
cla
hold on
plot(ts,CDKBTs,'linewidth',1.2)
plot(ts,MYB3R3s,'linewidth',1.2)
legend('CDKA/B:CYCB','MYB3R3','location','northeast','interpreter','latex')
xlabel('Time since G1/S /Units','interpreter','latex')
ylabel('Protein concentration','interpreter','latex')
title('\textbf{A}','interpreter','latex')

    function dydt=F(~,y)
        
        CDKBT=y(1);

        
        DCDKBT = scdkb - dcdkb*CDKBT;
        
        dydt=[DCDKBT];
    end

    function out=Trace(arg1,arg2,arg3)
        out=arg1+arg2+arg3;
    end

    function out=compl(arg1,arg2,arg3)
        out=2.*arg1.*arg2./(Trace(arg1,arg2,arg3)+sqrt(Trace(arg1,arg2,arg3).^2-4.*arg1.*arg2));
        %out=(Trace(arg1,arg2,arg3)+sqrt(Trace(arg1,arg2,arg3).^2-4.*arg1.*arg2));
    end

end