dt=1e-3;

cellNos=numel(CellData);

hold on

l=lines;
c1=l(1,:);
c2=l(2,:);

p=3;

for cellNo=1:cellNos

    ts=[]; ys=[];
    TS0=[];
    TS=[];
    YS={};
    while cellNo>0

        ts=CellData(cellNo).TS; 
        ys=CellData(cellNo).YS; 
        
        
        TS0=[TS0;ts(1)];
        TS=[TS,{ts}];
        YS=[YS,{ys}];

        cellNo=CellData(cellNo).parent;
    end

    [TS0s,Is]=sort(TS0,'ascend');
    TSs=TS(Is);
    YSs=YS(Is);

    nCL=numel(TSs);

    ts=[];
    Ts=[];
    Ys=[];
    for k=1:nCL
       tsNew=TSs{k}; 

       if ~isempty(ts)
           tsNew(1)=ts(end)+dt;
       end
       ts=tsNew;

       ys=YSs{k};

       Ts=[Ts;ts];
       Ys=[Ys;ys];

    end
    
    p1=plot(Ts,Ys(:,p),'color',c1,'linewidth',1.2);
    %plot(ts(1)+tsPhase,ysPhase,'x','color',c)
end

hold on
for cellNo=1:cellNos
    tsPhase=CellData(cellNo).tsPhase;
    ysPhase=CellData(cellNo).ysPhase;
    ts=CellData(cellNo).TS; 
    ys=CellData(cellNo).YS; 
    
    ind=(ts>=tsPhase(1))&(ts<tsPhase(2));
   
    p2=plot(ts(ind),ys(ind,p),'color',c2,'linewidth',1.2);
end

legend([p1 p2],'S-G2-M','G1','interpreter','latex','location','northwest')
xlim([0 4000])
xlabel('Time','interpreter','latex')
ylabel('KRP Concentration','interpreter','latex')