%figure;

%subplot(2,1,1)
hold on

t1=1;
t2=2;

nCells=numel(CellData);

BirthSizes=nan(nCells,1);
DivisionSizes=nan(nCells,1);
tCellCycle=nan(nCells,1);

for cellNo=1:nCells
    ysPhase = CellData(cellNo).ysPhase;
    tsPhase = CellData(cellNo).tsPhase;
    VsPhase = ysPhase(:,1);
    BirthSizes(cellNo)=VsPhase(t1);
    DivisionSizes(cellNo)=VsPhase(t2);
    tCellCycle(cellNo)=tsPhase(t2)-tsPhase(t1);
end

%plot(BirthSizes,DivisionSizes-BirthSizes,'x')
X=BirthSizes;

Y=DivisionSizes-BirthSizes;
%Y=tCellCycle;

c=lines;

p1=plot(X(2:end),Y(2:end),'x','color',c(1,:));


%figure;

%subplot(2,1,1)
hold on

t1=2;
t2=4;

nCells=numel(CellData);

BirthSizes=nan(nCells,1);
DivisionSizes=nan(nCells,1);
tCellCycle=nan(nCells,1);

for cellNo=1:nCells
    ysPhase = CellData(cellNo).ysPhase;
    tsPhase = CellData(cellNo).tsPhase;
    VsPhase = ysPhase(:,1);
    BirthSizes(cellNo)=VsPhase(t1);
    DivisionSizes(cellNo)=VsPhase(t2);
    tCellCycle(cellNo)=tsPhase(t2)-tsPhase(t1);
end

%plot(BirthSizes,DivisionSizes-BirthSizes,'x')
X=BirthSizes;

Y=DivisionSizes-BirthSizes;
%Y=tCellCycle;

c=lines;

p2=plot(X(2:end),Y(2:end),'x','color',c(2,:));



xlabel('Birth volume /Volume','interpreter','latex')
ylabel('Added volume /Volume','interpreter','latex')

grid minor


l2=legend([p1 p2],'Birth-G1/S','G1/S-Division','interpreter','latex','location','east');