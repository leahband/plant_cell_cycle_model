function [StdSts,Stab]=G1SBifurcation(CdkAT);

kskrp=0.01; 
kdkrpa=0.01; 
kdkrp=1;
RbT=2; 
kprb=1; 
kdprb=0.25;
E2FT=1; 
KdRE=0.001;
ks17a=0; 
ks17=0.1; 
kd17=0.1; 
kdAK=0.01;

npts=1000;
KRPT=linspace(0,1,npts);

%
CdkAKRP = compl(KRPT,CdkAT,kdAK);
CdkA = CdkAT - CdkAKRP;
RbP = RbT.*(kprb).*CdkA./((kprb).*CdkA + kdprb);
Rb = RbT - RbP;
RbE2F = compl(Rb,E2FT,KdRE);
E2F = E2FT - RbE2F;
FBL17 = (ks17a + ks17.*E2F)./kd17;
DKRPT = kskrp - (kdkrpa + kdkrp.*FBL17).*KRPT;
%
crsng=find(sign(DKRPT(2:end))~=sign(DKRPT(1:end-1)));

StdSts=[];
Stab=[];
if ~isempty(crsng)
    N=length(crsng);
    StdSts=nan(N,1);
    Stab=nan(N,1);
    for k=1:N
        crs=crsng(k);
        x1=KRPT(crs); x2=KRPT(crs+1);
        y1=DKRPT(crs); y2=DKRPT(crs+1);
        m=(y2-y1)/(x2-x1);
        c=y1-m*x1;
        KRPT0=-c/m;
        StdSts(k)=KRPT0;
        Stab(k)=sign(m);
    end
end

    function out=Trace(arg1,arg2,arg3)
        out=arg1+arg2+arg3;
    end

    function out=compl(arg1,arg2,arg3)
        out=2.*arg1.*arg2./(Trace(arg1,arg2,arg3)+sqrt(Trace(arg1,arg2,arg3).^2-4.*arg1.*arg2));
        %out=(Trace(arg1,arg2,arg3)+sqrt(Trace(arg1,arg2,arg3).^2-4.*arg1.*arg2));
    end
    
end
