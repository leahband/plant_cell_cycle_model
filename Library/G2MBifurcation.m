function [StdSts,Stab]=G2MBifurcation(CDKBT);

MYB3R3T=2;


smyb3=0.01;
dmyb3=0.01;
kpmyb3 = 1;%0.9058;
kdpmyb3=0.25;%0.0317;

npts=1000;

MYB3R3=linspace(0,1,npts);

CDKB=CDKBT;
f = kdpmyb3/(CDKB + kdpmyb3);

DMYB3R3 = f*(smyb3) - dmyb3*MYB3R3;

crsng=find(sign(DMYB3R3(2:end))~=sign(DMYB3R3(1:end-1)));
StdSts=[];
Stab=[];

if ~isempty(crsng)
    N=length(crsng);
    StdSts=nan(N,1);
    Stab=nan(N,1);
    for k=1:N
        crs=crsng(k);
        x1=MYB3R3(crs); x2=MYB3R3(crs+1);
        y1=DMYB3R3(crs); y2=DMYB3R3(crs+1);
        m=(y2-y1)/(x2-x1);
        c=y1-m*x1;
        x0=-c/m;
        StdSts(k)=x0;
        Stab(k)=sign(m);
    end
end

    function out=Trace(arg1,arg2,arg3)
        out=arg1+arg2+arg3;
    end

    function out=compl(arg1,arg2,arg3)
        out=2.*arg1.*arg2./(Trace(arg1,arg2,arg3)+sqrt(Trace(arg1,arg2,arg3).^2-4.*arg1.*arg2));
        %out=(Trace(arg1,arg2,arg3)+sqrt(Trace(arg1,arg2,arg3).^2-4.*arg1.*arg2));
    end
    
end
