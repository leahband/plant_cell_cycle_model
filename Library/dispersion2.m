function CDs=dispersion2(CellData)

try 
    t1=1;
    t2=2;
    t3=4;

    nCells=numel(CellData);
    BirthSizes=nan(nCells,1);
    G1SSizes=nan(nCells,1);
    DivisionSizes=nan(nCells,1);

    for cellNo=1:nCells
        ysPhase = CellData(cellNo).ysPhase;
        VsPhase = ysPhase(:,1);
        BirthSizes(cellNo)=VsPhase(t1);
        G1SSizes(cellNo)=VsPhase(t2);
        DivisionSizes(cellNo)=VsPhase(t3);
    end

    QBirth=quantile(BirthSizes,3);
    CDBirth=(QBirth(3)-QBirth(1))/(QBirth(1)+QBirth(1));

    QG1S=quantile(G1SSizes,3);
    CDG1S=(QG1S(3)-QG1S(1))/(QG1S(1)+QG1S(1));

    QDivision=quantile(DivisionSizes,3);
    CDDivision=(QDivision(3)-QDivision(1))/(QDivision(1)+QDivision(1));

    CDs=[CDBirth,CDG1S,CDDivision];
catch
    CDs=nan(1,3) 
    'failure'
end
end