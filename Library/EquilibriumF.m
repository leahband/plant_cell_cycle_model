function [E2FAs,Myb3As]=EquilibriumF(TS,YS)


kskrp=0.01/2; 
kskrpa=.01;

kdcdka1=0.00; %0.06, 0.1
kdcdka2=.5;

ksSCF=0.01;
kdSCF=0.01;

ksE2FB=0.01;
kdE2FB=0.01;

%G2M parameters

kscdkb=0.01/0.6;
kdcdkb=0.01;
kdcdkb1=.1;

kpmyb4=2;
kdpmyb4=0.25; 

ksmyb4a=0.01;
ksmyb4=0.11;
kdmyb4a=0.1;

ksAPC=0.01;
kdAPC=0.01;

kscdka=0.01;
kdcdka=0.01;
kdkrpa=0.01; 
kdkrp=1;
RbT=2; 
kprb=1; 
kdprb=0.25;
E2FT=1; 
KdRE=0.001;
ks17a=0; 
ks17=0.1; 
kd17=0.1; 
kdAK=0.01;

ssmr=0.01;
dsmr=0.01;
dpsmr=0.1;

kd=0.001;
kpd=.25;

Myb3T=1;

tpts=length(TS);
E2FAs=zeros(size(tpts));
Myb3As=E2FAs;
for k=1:tpts
    y=YS(k,:);
    t=TS(k);
    [E2FA,Myb3A]=Equilibria(t,y);
    E2FAs(k)=E2FA;
    Myb3As(k)=Myb3A;
end

    function [E2FA,Myb3A]=Equilibria(t,y)
        V=y(1);
        CdkAT=y(2);
        KRPT=y(3);
        E2FB=y(4);
        SCF=y(5);
        CdkBT=y(6);
        SMRT=y(7);
        Myb4T=y(8);
        APC=y(9);
        
        %G1/S Equilibrium
        CdkAKRP = compl(KRPT,CdkAT,kdAK);
        CdkA = CdkAT - CdkAKRP;
        RbP = RbT.*(kprb).*CdkA./((kprb).*CdkA + kdprb);
        Rb = RbT - RbP;
        RbE2F = compl(Rb,E2FT,KdRE);
        E2F = E2FT - RbE2F;
        FBL17 = (ks17a + ks17.*E2F)./kd17;
                
        %G2/M equilibrium
        CdkBSMR = compl(CdkBT,SMRT,kd);
        CdkB = CdkBT - CdkBSMR;
        pSMR = SMRT.*(CdkB./(CdkB + kpd));
        Myb4P = Myb4T.*(kpmyb4).*(CdkB)./((kpmyb4).*CdkB + kdpmyb4);
        Myb3P = Myb3T.*(kpmyb4).*(CdkB)./((kpmyb4).*CdkB + kdpmyb4);
        
        E2FA=E2F/E2FT;
        Myb3A=(Myb3T-Myb3P)/Myb3T;
    end


 
%% Function hell

    function out=Trace(arg1,arg2,arg3)
        out=arg1+arg2+arg3;
    end

    function out=compl(arg1,arg2,arg3)
        out=2.*arg1.*arg2./(Trace(arg1,arg2,arg3)+sqrt(Trace(arg1,arg2,arg3).^2-4.*arg1.*arg2));
        %out=(Trace(arg1,arg2,arg3)+sqrt(Trace(arg1,arg2,arg3).^2-4.*arg1.*arg2));
    end
end