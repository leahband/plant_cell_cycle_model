function SingleCellCycle_NoEvent

t0=0;
y0=[0.5    0.0881    0.4642    0.2718    0.4507    0.2891    0.2033    0.3662    0.2111]';

KRP_SI=false;
SMR_SI=false;

if isrow(y0)
    y0=y0';
end

terminationTime=10000;

DNF=false;
hasReachedG1S=false;
hasReachedG2M=false;

rgr=6.6649e-04;

%%%%%%%%%%%%%%%%%%%%%%%%%%

%synthesis

kscdka=0.01; %0.85,10;
kskrp=(0.01/1.0); %0.1  1.21;
kskrpa=.01; %0.1 10;
ksSCF=0.01; %0.1 10;
kscdkb=(0.01/0.6*100); %0.83 10;
ksE2FB=0.01; %0.83 10;
ksmyb4a=0.01; %0.1 3.8;
ksmyb4=0.11; %0.1 10;
ksAPC=0.01; %0.1 3.86;
ks17a=0; 
ks17=0.1; %0.28 10;
ssmr=0.01; %0.1 1.21];

%degradation

kdcdka=0.01; %[0.1 1.18;
kdcdka1=0; 
kdcdka2=.5;  %0.1 8.4; 
kdSCF=0.01; % 0.1 10;
kdE2FB=0.01;   %0.1 1.21;                   Very cool 0.1
kdcdkb=0.01; %0.1 1.21;
kdcdkb1=.1; %0.1 6.27;
kdmyb4a=0.1; %0.285 3.66;
kdAPC=0.01; %0.1 10;
kd17=0.1; %0.1 3.64;
kdkrpa=0.01; %0.78 10;
kdkrp=1; %0.28 10;
dsmr=0.01; %0.8 10;
dpsmr=0.1; %0.42 10];

%phosphorylation

kdprb=(0.25/1); %[0.1 1.3;
kdpmyb3=(0.25/2); %0.1 10;
kdpmyb4=(0.25/2); %0.27 10
kpd=.25; %0.1 3.05]; 

%reaction rates

KdRE=0.001; %[0.27 10; 
kdAK=0.01; %0.1 10; 
kd=0.001; %0.1 10];

%%%%%%%%%%%%%%%%%%%%%%%%%%


kicdkb=100;

RbT=2; 
E2FT=1; 
Myb3T=1;

%% ODE initialisation

tspan=terminationTime;

options = odeset('Events',@EventsFcn);

tc=t0;
yc=y0;

TS=tc;
YS=yc';

tsPhase=nan(4,1);
ysPhase=nan(4,length(yc));
Phase={'Birth','G1/S','G2/M','Cytokinesis'};

tsPhase(1)=tc;
ysPhase(1,:)=yc';

HasG1S=false;
HasG2M=false;
%% ODE


[ts,ys]=ode45(@model,[tc tc+tspan],yc);
TS=ts;
YS=ys;

plot(TS,YS(:,2:end))

%[E2FAs,Myb3As]=Equilibria2(TS,YS);
%size(E2FAs)
%plot(TS,Myb3As)

%% ODE functions

    function dydt=model(~,y)
        V=y(1);
        CdkAT=y(2);
        KRPT=y(3);
        E2FB=y(4);
        SCF=y(5);
        CdkBT=y(6);
        SMRT=y(7);
        Myb4T=y(8);
        APC=y(9);
        
        %G1/S Equilibrium
        CdkAKRP = compl(KRPT,CdkAT,kdAK);
        CdkA = CdkAT - CdkAKRP;
        RbP = RbT.*CdkA./(CdkA + kdprb);
        Rb = RbT - RbP;
        RbE2F = compl(Rb,E2FT,KdRE);
        E2F = E2FT - RbE2F;
        FBL17 = (ks17a + ks17.*E2F)./kd17;
                
        %G2/M equilibrium
        CdkBSMR = compl(CdkBT,SMRT,kd);
        CdkB = CdkBT - CdkBSMR;
        pSMR = SMRT.*(CdkB./(CdkB + kpd));
        Myb4P = Myb4T.*(CdkB)./(CdkB + kdpmyb4);
        Myb3P = Myb3T.*(CdkB)./(CdkB + kdpmyb4);
        Myb3U=Myb3T-Myb3P;
        
        %G1/S dynamics
        DCdkAT = kscdka - (kdcdka + kdcdka1*SCF +kdcdka2*APC)*CdkAT;
        
        if KRP_SI
            DKRPT = 10*( (kskrp +kskrpa*Myb4P)/V - (kdkrpa + kdkrp*FBL17)*KRPT );
        else
            DKRPT=(kskrp +kskrpa*Myb4P) - (kdkrpa + kdkrp*FBL17)*KRPT;
        end
        %DKRPT = kskrp - (kdkrpa + kdkrp.*FBL17).*KRPT;
        DE2FBT= E2F*ksE2FB - kdE2FB*E2FB;
        DSCF = E2FB*ksSCF - kdSCF*SCF;
        
        %G2/M dynamics
        DCdkBT = kscdkb*E2FB/(kicdkb + Myb3U) - (kdcdkb + kdcdkb1*APC)*CdkBT; %degraded by APC
        
        if SMR_SI
            DSMRT=10*(ssmr/V - dsmr*(SMRT-pSMR) - dpsmr*pSMR); 
        else
            DSMRT=ssmr - dsmr*(SMRT-pSMR) - dpsmr*pSMR; 
        end
        DMyb4T  = ksmyb4a + ksmyb4*Myb4P - kdmyb4a*Myb4T;
        DAPC = ksAPC*Myb4P - kdAPC*APC;       
        
        DV = rgr*V;
        
        dydt=[DV;DCdkAT;DKRPT;DE2FBT;DSCF;DCdkBT;DSMRT;DMyb4T;DAPC];
    end

%% Events function

    function [position,isterminal,direction] = EventsFcn(t,y)
        [E2FA,Myb3A]=Equilibria(t,y);   
        position = [E2FA-0.25,Myb3A-0.5]; 
        isterminal = [1,1];  
        direction = [0,0];   
    end

    function [E2FA,Myb3A]=Equilibria(t,y)
        V=y(1);
        CdkAT=y(2);
        KRPT=y(3);
        E2FB=y(4);
        SCF=y(5);
        CdkBT=y(6);
        SMRT=y(7);
        Myb4T=y(8);
        APC=y(9);
        
        %G1/S Equilibrium
        CdkAKRP = compl(KRPT,CdkAT,kdAK);
        CdkA = CdkAT - CdkAKRP;
        RbP = RbT.*(kprb).*CdkA./((kprb).*CdkA + kdprb);
        Rb = RbT - RbP;
        RbE2F = compl(Rb,E2FT,KdRE);
        E2F = E2FT - RbE2F;
        FBL17 = (ks17a + ks17.*E2F)./kd17;
                
        %G2/M equilibrium
        CdkBSMR = compl(CdkBT,SMRT,kd);
        CdkB = CdkBT - CdkBSMR;
        pSMR = SMRT.*(CdkB./(CdkB + kpd));
        Myb4P = Myb4T.*(CdkB)./(CdkB + kdpmyb4);
        Myb3P = Myb3T.*(CdkB)./(CdkB + kdpmyb3);
        
        E2FA=E2F/E2FT;
        Myb3A=(Myb3T-Myb3P)/Myb3T;
    end


 
%% Function hell

    function out=Trace(arg1,arg2,arg3)
        out=arg1+arg2+arg3;
    end

    function out=compl(arg1,arg2,arg3)
        out=2.*arg1.*arg2./(Trace(arg1,arg2,arg3)+sqrt(Trace(arg1,arg2,arg3).^2-4.*arg1.*arg2));
        %out=(Trace(arg1,arg2,arg3)+sqrt(Trace(arg1,arg2,arg3).^2-4.*arg1.*arg2));
    end
end