function CellData=MultiCellModel(KRP_SI,SMR_SI,eq_inh,params);

%probability distribution
mu=0.5;
sigma=0.05;
norm = makedist('normal');
norm.mu=mu;
norm.sigma=sigma;
normt=truncate(norm,0,1);

%--------------------------------------------------------------------------

doLoop=true;

nCells=128;
t0=0;
Y0=[0.5    0.0881    0.4642    0.2718    0.4507    0.2891    0.2033    0.3662    0.2111]';

cellsDone=0;
cellNo=1;

t0s={t0};
Y0s={Y0}; 
parents={0};

CellData=struct;
CellData.TS=[]; CellData.YS=[]; CellData.tsPhase=[]; CellData.ysPhase=[]; CellData.parent=[];

    while doLoop
        
        t0=t0s{1}; Y0=Y0s{1}; parent=parents{1};
        
        %[TS,YS,tsPhase,ysPhase,DNF]=SingleCellCycle(t0,Y0,KRP_SI,SMR_SI,params);
        %[TS,YS,tsPhase,ysPhase,DNF]=SingleCellCycle_KRP(t0,Y0,KRP_SI,SMR_SI,params); 
        [TS,YS,tsPhase,ysPhase,DNF]=SingleCellCycle_KRPPhenom(t0,Y0,KRP_SI,SMR_SI,params);
        
        if DNF
            break
        else
            %record data
            CellData(cellNo).TS=TS; 
            CellData(cellNo).YS=YS; 
            CellData(cellNo).tsPhase=tsPhase; 
            CellData(cellNo).ysPhase=ysPhase;
            CellData(cellNo).parent=parent;
            
            yEnd=YS(end,:);
            tEnd=TS(end);
            
            VEnd=yEnd(1);
            
            
            [Y1,Y2]=CellDivision(yEnd);
            
            t0s=[t0s,{tEnd},{tEnd}];
            Y0s=[Y0s,{Y1},{Y2}];
            parents=[parents,{cellNo},{cellNo}];
            
            t0s=t0s(2:end); %remove the cell we've just done
            Y0s=Y0s(2:end);
            parents=parents(2:end);
            
            cellNo=cellNo+1;
            cellsDone=cellsDone+1;
            
            
        end
        
        doLoop=cellsDone<nCells;
        
    end

    function [y1,y2]=CellDivision(y)
        %assumption: distribute concentration equally
        y1=y;
        y2=y;
                
        %distribute volume randomly
        d=random(normt);
        
        %volume
        V0=y(1);
        V1=d*V0;
        V2=V0-V1;
        y1(1)=V1;
        y2(1)=V2;
        
        %concentration
        
        if ~isempty(eq_inh)
        
            C0=y(eq_inh);

            C1=C0*V0/(2*V1);
            C2=C0*V0/(2*V2);

            y1(eq_inh)=C1;
            y2(eq_inh)=C2;
        
        end

    end
    
    
end