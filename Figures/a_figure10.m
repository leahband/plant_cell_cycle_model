figure;

load('KRP_EI_SMR_SI.mat');

subplot(3,2,1)

hold on

t2=2;

nCells=numel(CellData);

BirthSizes=nan(nCells,1);
DivisionSizes=nan(nCells,1);
tCellCycle=nan(nCells,1);

for cellNo=1:nCells
    ysPhase = CellData(cellNo).ysPhase;
    tsPhase = CellData(cellNo).tsPhase;
    VsPhase = ysPhase(:,1);
    BirthSizes(cellNo)=VsPhase(1);
    DivisionSizes(cellNo)=VsPhase(t2);
    tCellCycle(cellNo)=tsPhase(t2)-tsPhase(1);
end

%plot(BirthSizes,DivisionSizes-BirthSizes,'x')
X=BirthSizes;

%Y=DivisionSizes-BirthSizes;
Y=tCellCycle;

c=lines;

plot(X(2:end),Y(2:end),'x','color',c(1,:))

load('KRP_EIMyb4_SMR_SI.mat');

hold on

t2=2;

nCells=numel(CellData);

BirthSizes=nan(nCells,1);
DivisionSizes=nan(nCells,1);
tCellCycle=nan(nCells,1);

for cellNo=1:nCells
    ysPhase = CellData(cellNo).ysPhase;
    tsPhase = CellData(cellNo).tsPhase;
    VsPhase = ysPhase(:,1);
    BirthSizes(cellNo)=VsPhase(1);
    DivisionSizes(cellNo)=VsPhase(t2);
    tCellCycle(cellNo)=tsPhase(t2)-tsPhase(1);
end

%plot(BirthSizes,DivisionSizes-BirthSizes,'x')
X=BirthSizes;

%Y=DivisionSizes-BirthSizes;
Y=tCellCycle;

c=lines;

plot(X(2:end),Y(2:end),'x','color',c(2,:))

grid minor

xlabel('Birth volume /Volume','interpreter','latex')
ylabel('Phase duration /Time','interpreter','latex')
legend('Continual KRP','Phase-dependent KRP','interpreter','latex')
grid minor
%%



load('KRP_EI_SMR_SI.mat');

subplot(3,2,2)

hold on

t2=2;

nCells=numel(CellData);

BirthSizes=nan(nCells,1);
DivisionSizes=nan(nCells,1);
tCellCycle=nan(nCells,1);

for cellNo=1:nCells
    ysPhase = CellData(cellNo).ysPhase;
    tsPhase = CellData(cellNo).tsPhase;
    VsPhase = ysPhase(:,1);
    BirthSizes(cellNo)=VsPhase(1);
    DivisionSizes(cellNo)=VsPhase(t2);
    tCellCycle(cellNo)=tsPhase(t2)-tsPhase(1);
end

%plot(BirthSizes,DivisionSizes-BirthSizes,'x')
X=BirthSizes;

Y=DivisionSizes-BirthSizes;
%Y=tCellCycle;

c=lines;

p1=plot(X(2:end),Y(2:end),'x','color',c(1,:));
P = polyfit(X,Y,1);
Yp = polyval(P,X);
hold on; plot(X,Yp,'linewidth',1.2,'color',c(1,:))



load('KRP_EIMyb4_SMR_SI.mat');

hold on

t2=2;

nCells=numel(CellData);

BirthSizes=nan(nCells,1);
DivisionSizes=nan(nCells,1);
tCellCycle=nan(nCells,1);

for cellNo=1:nCells
    ysPhase = CellData(cellNo).ysPhase;
    tsPhase = CellData(cellNo).tsPhase;
    VsPhase = ysPhase(:,1);
    BirthSizes(cellNo)=VsPhase(1);
    DivisionSizes(cellNo)=VsPhase(t2);
    tCellCycle(cellNo)=tsPhase(t2)-tsPhase(1);
end

%plot(BirthSizes,DivisionSizes-BirthSizes,'x')
X=BirthSizes;

Y=DivisionSizes-BirthSizes;
%Y=tCellCycle;

c=lines;

p2=plot(X(2:end),Y(2:end),'x','color',c(2,:));
P = polyfit(X,Y,1);
Yp = polyval(P,X);
hold on; plot(X,Yp,'linewidth',1.2,'color',c(2,:))

xlabel('Birth volume /Volume','interpreter','latex')
ylabel('Added volume /Volume','interpreter','latex')
legend([p1 p2],'Continual KRP','Phase-dependent KRP','interpreter','latex','location','northwest')

subplot(3,2,1)
title('\textbf{A}','interpreter','latex')
grid minor
subplot(3,2,2)
title('\textbf{B}','interpreter','latex')
grid minor

%%
subplot(3,1,2)

dt=1e-3;

cellNos=numel(CellData);

hold on

l=lines;
c1=l(1,:);
c2=l(2,:);

p=3;

for cellNo=1:cellNos

    ts=[]; ys=[];
    TS0=[];
    TS=[];
    YS={};
    while cellNo>0

        ts=CellData(cellNo).TS; 
        ys=CellData(cellNo).YS; 
        
        
        TS0=[TS0;ts(1)];
        TS=[TS,{ts}];
        YS=[YS,{ys}];

        cellNo=CellData(cellNo).parent;
    end

    [TS0s,Is]=sort(TS0,'ascend');
    TSs=TS(Is);
    YSs=YS(Is);

    nCL=numel(TSs);

    ts=[];
    Ts=[];
    Ys=[];
    for k=1:nCL
       tsNew=TSs{k}; 

       if ~isempty(ts)
           tsNew(1)=ts(end)+dt;
       end
       ts=tsNew;

       ys=YSs{k};

       Ts=[Ts;ts];
       Ys=[Ys;ys];

    end
    
    p1=plot(Ts,Ys(:,p),'color',c1,'linewidth',1.2);
    %plot(ts(1)+tsPhase,ysPhase,'x','color',c)
end

hold on
for cellNo=1:cellNos
    tsPhase=CellData(cellNo).tsPhase;
    ysPhase=CellData(cellNo).ysPhase;
    ts=CellData(cellNo).TS; 
    ys=CellData(cellNo).YS; 
    
    ind=(ts>=tsPhase(1))&(ts<tsPhase(2));
   
    p2=plot(ts(ind),ys(ind,p),'color',c2,'linewidth',1.2);
end

legend([p1 p2],'S-G2-M','G1','interpreter','latex','location','northwest')
xlim([0 4000])
xlabel('Time','interpreter','latex')
ylabel('KRP Concentration','interpreter','latex')
title('\textbf{C}','interpreter','latex')
%%
subplot(3,1,3)
dt=1e-3;

cellNos=numel(CellData);

hold on

l=lines;
c1=l(1,:);
c2=l(2,:);

for cellNo=1:cellNos

    ts=[]; ys=[];
    TS0=[];
    TS=[];
    YS={};
    while cellNo>0

        ts=CellData(cellNo).TS; 
        ys=CellData(cellNo).YS; 
        
        
        TS0=[TS0;ts(1)];
        TS=[TS,{ts}];
        YS=[YS,{ys}];

        cellNo=CellData(cellNo).parent;
    end

    [TS0s,Is]=sort(TS0,'ascend');
    TSs=TS(Is);
    YSs=YS(Is);

    nCL=numel(TSs);

    ts=[];
    Ts=[];
    Ys=[];
    for k=1:nCL
       tsNew=TSs{k}; 

       if ~isempty(ts)
           tsNew(1)=ts(end)+dt;
       end
       ts=tsNew;

       ys=YSs{k};

       Ts=[Ts;ts];
       Ys=[Ys;ys];

    end
    
    plot(Ts,Ys(:,1),'color',[.5 .5 .5])
    %plot(ts(1)+tsPhase,ysPhase,'x','color',c)
end

hold on
for cellNo=1:cellNos
    ts=CellData(cellNo).TS; 
    tsPhase=CellData(cellNo).tsPhase;
    ysPhase=CellData(cellNo).ysPhase;
    plot(tsPhase(2),ysPhase(2),'x','color',c1)    
    plot(tsPhase(4),ysPhase(4),'x','color',c2) 
end


p1=plot(tsPhase(2),ysPhase(2),'x','color',c1);   
p2=plot(tsPhase(4),ysPhase(4),'x','color',c2); 

legend([p1 p2],'G1/S','Division','interpreter','latex','location','southeast')
xlabel('Time','interpreter','latex')
ylabel('Volume','interpreter','latex')
title('\textbf{D}','interpreter','latex')