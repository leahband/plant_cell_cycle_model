function [TS,YS,tsPhase,ysPhase,DNF]=SingleCellCycle(t0,y0)

if isrow(y0)
    y0=y0';
end

terminationTime=10999;

DNF=false;
hasReachedG1S=false;
hasReachedG2M=false;

rgr=6.6649e-04;

kskrp=0.01; 
kskrpa=.00;

kdcdka1=0.00; %0.06, 0.1
kdcdka2=.5;

ksSCF=0.01;
kdSCF=0.01;

ksE2FB=0.01;
kdE2FB=0.01;

%G2M parameters

kscdkb=0.01/0.6;
kdcdkb=0.01;
kdcdkb1=.1;

kpmyb4=2;
kdpmyb4=0.25; 

ksmyb4a=0.01;
ksmyb4=0.11;
kdmyb4a=0.1;

ksAPC=0.01;
kdAPC=0.01;

kscdka=0.01;
kdcdka=0.01;
kdkrpa=0.01; 
kdkrp=1;
RbT=2; 
kprb=1; 
kdprb=0.25;
E2FT=1; 
KdRE=0.001;
ks17a=0; 
ks17=0.1; 
kd17=0.1; 
kdAK=0.01;

ssmr=0.01;
dsmr=0.01;
dpsmr=0.1;

kd=0.001;
kpd=.25;

Myb3T=1;

%% ODE initialisation

tspan=500;

options = odeset('Events',@EventsFcn);

tc=t0;
yc=y0;

TS=tc;
YS=yc';

tsPhase=nan(4,1);
ysPhase=nan(4,length(yc));
Phase={'Birth','G1/S','G2/M','Cytokinesis'};

tsPhase(1)=tc;
ysPhase(1,:)=yc';

HasG1S=false;
HasG2M=false;
%% ODE

terminateCondition=false;
while ~terminateCondition
    [ts,ys]=ode45(@model,[tc tc+tspan],yc,options);
    tc=ts(end);
    yc=ys(end,:)';

    TS=[TS;ts(2:end)];
    YS=[YS;ys(2:end,:)];
    
    [E2FA_,Myb3A_]=Equilibria(tc,yc);
        
    G1S = (E2FA_>0.25);
    G2M = (Myb3A_<0.5);
    
    if G1S&&(~HasG1S)
        HasG1S = true;
        tsPhase(2)=tc;
        ysPhase(2,:)=yc';
    end
    
    if G2M&&(~HasG2M)
        HasG2M = true;
        tsPhase(3)=tc;
        ysPhase(3,:)=yc';
    end
    
    Cytokinesis = HasG2M&&(G1S<0.25);
    
    if Cytokinesis
        tsPhase(4)=tc;
        ysPhase(4,:)=yc';
    end
    
    %figure; hold on; plot(SMRT_,zeros(size(SMRT_)),'k'); plot(SMRT_,DSMRT_);
    terminateCondition = Cytokinesis||(tc>=terminationTime);    
end

if tc>=terminationTime
    TS=nan;
    YS=nan;
    DNF=true;
end

%[E2FAs,Myb3As]=Equilibria2(TS,YS);
%size(E2FAs)
%plot(TS,Myb3As)

%% ODE functions

    function dydt=model(~,y)
        CdkAT=y(1);
        KRPT=y(2);
        E2FB=y(3);
        SCF=y(4);
        CdkBT=y(5);
        SMRT=y(6);
        Myb4T=y(7);
        APC=y(8);
        
        %G1/S Equilibrium
        CdkAKRP = compl(KRPT,CdkAT,kdAK);
        CdkA = CdkAT - CdkAKRP;
        RbP = RbT.*(kprb).*CdkA./((kprb).*CdkA + kdprb);
        Rb = RbT - RbP;
        RbE2F = compl(Rb,E2FT,KdRE);
        E2F = E2FT - RbE2F;
        FBL17 = (ks17a + ks17.*E2F)./kd17;
                
        %G2/M equilibrium
        CdkBSMR = compl(CdkBT,SMRT,kd);
        CdkB = CdkBT - CdkBSMR;
        pSMR = SMRT.*(CdkB./(CdkB + kpd));
        Myb4P = Myb4T.*(kpmyb4).*(CdkB)./((kpmyb4).*CdkB + kdpmyb4);
        Myb3P = Myb3T.*(kpmyb4).*(CdkB)./((kpmyb4).*CdkB + kdpmyb4);
        
        %G1/S dynamics
        DCdkAT = kscdka - (kdcdka + kdcdka1*SCF +kdcdka2*APC)*CdkAT;
        DKRPT = kskrp +kskrpa*Myb4P - (kdkrpa + kdkrp*FBL17)*KRPT;
        %DKRPT = kskrp - (kdkrpa + kdkrp.*FBL17).*KRPT;
        DE2FBT= E2F*ksE2FB - kdE2FB*E2FB;
        DSCF = E2FB*ksSCF - kdSCF*SCF;
        
        %G2/M dynamics
        DCdkBT = kscdkb*E2FB - (kdcdkb + kdcdkb1*APC)*CdkBT; %degraded by APC
        DSMRT=ssmr - dsmr*(SMRT-pSMR) - dpsmr*pSMR;        
        DMyb4T  = ksmyb4a + ksmyb4*Myb4P - kdmyb4a*Myb4T;
        DAPC = ksAPC*Myb4P - kdAPC*APC;       
        
        dydt=[DCdkAT;DKRPT;DE2FBT;DSCF;DCdkBT;DSMRT;DMyb4T;DAPC];
    end

%% Events function

    function [position,isterminal,direction] = EventsFcn(t,y)
        [E2FA,Myb3A]=Equilibria(t,y);   
        position = [E2FA-0.25,Myb3A-0.5]; 
        isterminal = [1,1];  
        direction = [0,0];   
    end

    function [E2FA,Myb3A]=Equilibria(t,y)
        CdkAT=y(1);
        KRPT=y(2);
        E2FB=y(3);
        SCF=y(4);
        CdkBT=y(5);
        SMRT=y(6);
        Myb4T=y(7);
        APC=y(8);
        
        %G1/S Equilibrium
        CdkAKRP = compl(KRPT,CdkAT,kdAK);
        CdkA = CdkAT - CdkAKRP;
        RbP = RbT.*(kprb).*CdkA./((kprb).*CdkA + kdprb);
        Rb = RbT - RbP;
        RbE2F = compl(Rb,E2FT,KdRE);
        E2F = E2FT - RbE2F;
        FBL17 = (ks17a + ks17.*E2F)./kd17;
                
        %G2/M equilibrium
        CdkBSMR = compl(CdkBT,SMRT,kd);
        CdkB = CdkBT - CdkBSMR;
        pSMR = SMRT.*(CdkB./(CdkB + kpd));
        Myb4P = Myb4T.*(kpmyb4).*(CdkB)./((kpmyb4).*CdkB + kdpmyb4);
        Myb3P = Myb3T.*(kpmyb4).*(CdkB)./((kpmyb4).*CdkB + kdpmyb4);
        
        E2FA=E2F/E2FT;
        Myb3A=(Myb3T-Myb3P)/Myb3T;
    end

%% Function hell

    function out=Trace(arg1,arg2,arg3)
        out=arg1+arg2+arg3;
    end

    function out=compl(arg1,arg2,arg3)
        out=2.*arg1.*arg2./(Trace(arg1,arg2,arg3)+sqrt(Trace(arg1,arg2,arg3).^2-4.*arg1.*arg2));
        %out=(Trace(arg1,arg2,arg3)+sqrt(Trace(arg1,arg2,arg3).^2-4.*arg1.*arg2));
    end
end
