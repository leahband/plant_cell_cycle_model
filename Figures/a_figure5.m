figure;
G2M_2
G2MBifurcationLoop2
G2MBifurcationLoop3
%%
function G2M_2

MYB3R3T=1;

scdkba=0.001;
scdkb=0.01;

dcdkb=0.01;
ssmr=0.01;
dsmr=0.01;
dpsmr=1;

kd=0.001;
kpd=.25;

kpmyb3=1;
kdpmyb3=0.25;
kdmyb3=10;

CDKBT0=0;
SMR0=1;

y0=[CDKBT0;SMR0];
tspan=500;
[ts,ys]=ode45(@F,[0 tspan],y0);

CDKBTs=ys(:,1);
SMRTs=ys(:,2);

CDKBSMRs = compl(CDKBTs,SMRTs,kd);

CDKBs = CDKBTs - CDKBSMRs;

pSMRs = SMRTs.*(CDKBs./(CDKBs + kpd));

MYB3R3ps = MYB3R3T.*(kpmyb3).*CDKBs./((kpmyb3).*CDKBs + kdpmyb3);
        
MYB3R3s=MYB3R3T-MYB3R3ps;

subplot(3,1,1)
cla
hold on
plot(ts,CDKBTs,'linewidth',1.2)
plot(ts,SMRTs,'linewidth',1.2)
plot(ts,MYB3R3s,'linewidth',1.2)
legend('CDKA/B:CYCB','SMR$_T$','uMYB3R3','location','northeast','interpreter','latex')
xlabel('Time since G1/S /Units','interpreter','latex')
ylabel('Protein concentration','interpreter','latex')
title('\textbf{A}','interpreter','latex')

    function dydt=F(~,y)
        
        CDKBT=y(1);
        SMRT=y(2);
        
        CDKBSMR = compl(CDKBT,SMRT,kd);

        CDKB = CDKBT - CDKBSMR;

        pSMR = SMRT.*(CDKB./(CDKB + kpd));
        
        MYB3R3p = MYB3R3T.*(kpmyb3).*CDKB./((kpmyb3).*CDKB + kdpmyb3);
        
        MYB3R3=MYB3R3T-MYB3R3p;
                
        DSMRT=ssmr - dsmr*(SMRT-pSMR) - dpsmr*pSMR;
        
        DCDKBT= (scdkba + scdkb.*(kdmyb3./(MYB3R3+kdmyb3))) - dcdkb*CDKBT;
                
        dydt=[DCDKBT;DSMRT];
    end

    function out=Trace(arg1,arg2,arg3)
        out=arg1+arg2+arg3;
    end

    function out=compl(arg1,arg2,arg3)
        out=2.*arg1.*arg2./(Trace(arg1,arg2,arg3)+sqrt(Trace(arg1,arg2,arg3).^2-4.*arg1.*arg2));
        %out=(Trace(arg1,arg2,arg3)+sqrt(Trace(arg1,arg2,arg3).^2-4.*arg1.*arg2));
    end

end
%%
function G2MBifurcationLoop2
n=1000;
CdkATs=linspace(0,1,n);

maxlengths=zeros(1,n);
for k=1:n
    [StdSts,Stab]=G2MBifurcation2(CdkATs(k));
    maxlengths(k)=numel(StdSts);
end

maxlength=max(maxlengths);
StdSts_=nan(maxlength,n);
Stab_=nan(maxlength,n);

for k=1:n
    [StdSts,Stab]=G2MBifurcation2(CdkATs(k));
    m=length(StdSts);
    if m>=1
        StdSts_(1:m,k)=StdSts';
        Stab_(1:m,k)=Stab;
    end
end

CdkATs_=repmat(CdkATs,maxlength,1);

idx=~isnan(StdSts_);
StdSts_=StdSts_(idx);
Stab_=Stab_(idx);
CdkATs_=CdkATs_(idx);

[StdSts_,idx]=sort(StdSts_,'ascend');
CdkATs_=CdkATs_(idx);
Stab_=Stab_(idx);

idx=Stab_>0;
StdSts_u=StdSts_(idx);
CdkATs_u=CdkATs_(idx);

subplot(3,1,2)
plot(CdkATs_,StdSts_,'linewidth',1.2)
hold on
plot(CdkATs_u,StdSts_u,'linewidth',1.2)
legend('Stable','Unstable','interpreter','latex','location','southwest')

xlabel('CDKA/B:CYCB','interpreter','latex')
ylabel('SMR','interpreter','latex')
title('\textbf{B}','interpreter','latex')
end
%%
function G2MBifurcationLoop3
n=1000;
CdkATs=linspace(0,1,n);

maxlengths=zeros(1,n);
for k=1:n
    [StdSts,Stab]=G2MBifurcation3(CdkATs(k));
    maxlengths(k)=numel(StdSts);
end

maxlength=max(maxlengths);
StdSts_=nan(maxlength,n);
Stab_=nan(maxlength,n);

for k=1:n
    [StdSts,Stab]=G2MBifurcation3(CdkATs(k));
    m=length(StdSts);
    if m>=1
        StdSts_(1:m,k)=StdSts';
        Stab_(1:m,k)=Stab;
    end
end

CdkATs_=repmat(CdkATs,maxlength,1);

idx=~isnan(StdSts_);
StdSts_=StdSts_(idx);
Stab_=Stab_(idx);
CdkATs_=CdkATs_(idx);

[StdSts_,idx]=sort(StdSts_,'ascend');
CdkATs_=CdkATs_(idx);
Stab_=Stab_(idx);

idx=Stab_>0;
StdSts_u=StdSts_(idx);
CdkATs_u=CdkATs_(idx);

subplot(3,1,3)
plot(CdkATs_,StdSts_,'linewidth',1.2)
hold on
plot(CdkATs_u,StdSts_u,'linewidth',1.2)
legend('Stable','Unstable','interpreter','latex','location','southwest')

xlabel('CDKA/B:CYCB','interpreter','latex')
ylabel('uMYB3R3','interpreter','latex')
title('\textbf{C}','interpreter','latex')

end
%%
function [StdSts,Stab]=G2MBifurcation2(CDKBT)

scdkb=0.01;
dcdkb=0.01;
ssmr=0.01;
dsmr=0.01;
dpsmr=0.1;

kd=0.001;
kpd=.25;

npts=1000;
SMRT=linspace(0,1,npts);

CDKBSMR = compl(CDKBT,SMRT,kd);

CDKB = CDKBT - CDKBSMR;

pSMR = SMRT.*(CDKB./(CDKB + kpd));

DSMRT=ssmr - dsmr*(SMRT-pSMR) - dpsmr*pSMR;

crsng=find(sign(DSMRT(2:end))~=sign(DSMRT(1:end-1)));
StdSts=[];
Stab=[];

if ~isempty(crsng)
    N=length(crsng);
    StdSts=nan(N,1);
    Stab=nan(N,1);
    for k=1:N
        crs=crsng(k);
        x1=SMRT(crs); x2=SMRT(crs+1);
        y1=DSMRT(crs); y2=DSMRT(crs+1);
        m=(y2-y1)/(x2-x1);
        c=y1-m*x1;
        SMR0=-c/m;
        StdSts(k)=SMR0;
        Stab(k)=sign(m);
    end
end

    function out=Trace(arg1,arg2,arg3)
        out=arg1+arg2+arg3;
    end

    function out=compl(arg1,arg2,arg3)
        out=2.*arg1.*arg2./(Trace(arg1,arg2,arg3)+sqrt(Trace(arg1,arg2,arg3).^2-4.*arg1.*arg2));
        %out=(Trace(arg1,arg2,arg3)+sqrt(Trace(arg1,arg2,arg3).^2-4.*arg1.*arg2));
    end
    
end

%%
function [uMyb3,Stab]=G2MBifurcation3(CDKBT)

Myb3T=1;

scdkb=0.01;
dcdkb=0.01;
ssmr=0.01;
dsmr=0.01;
dpsmr=0.1;

kd=0.001;
kpd=.25;

npts=1000;
SMRT=linspace(0,1,npts);

CDKBSMR = compl(CDKBT,SMRT,kd);

CDKB = CDKBT - CDKBSMR;

pSMR = SMRT.*(CDKB./(CDKB + kpd));

DSMRT=ssmr - dsmr*(SMRT-pSMR) - dpsmr*pSMR;

crsng=find(sign(DSMRT(2:end))~=sign(DSMRT(1:end-1)));
StdSts=[];
Stab=[];

if ~isempty(crsng)
    N=length(crsng);
    StdSts=nan(N,1);
    Stab=nan(N,1);
    for k=1:N
        crs=crsng(k);
        x1=SMRT(crs); x2=SMRT(crs+1);
        y1=DSMRT(crs); y2=DSMRT(crs+1);
        m=(y2-y1)/(x2-x1);
        c=y1-m*x1;
        SMR0=-c/m;
        StdSts(k)=SMR0;
        Stab(k)=sign(m);
    end
end

SMRT=StdSts;
CDKBSMR = compl(CDKBT,SMRT,kd);
CDKB = CDKBT - CDKBSMR;
pMyb3 = Myb3T.*(CDKB./(CDKB + kpd));
uMyb3 = Myb3T - pMyb3;

    function out=Trace(arg1,arg2,arg3)
        out=arg1+arg2+arg3;
    end

    function out=compl(arg1,arg2,arg3)
        out=2.*arg1.*arg2./(Trace(arg1,arg2,arg3)+sqrt(Trace(arg1,arg2,arg3).^2-4.*arg1.*arg2));
        %out=(Trace(arg1,arg2,arg3)+sqrt(Trace(arg1,arg2,arg3).^2-4.*arg1.*arg2));
    end
    
end