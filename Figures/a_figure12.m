load('mutantdata.mat')

h=2;
v=2;
theta=20;

figure;
subplot(v,h,1)
boxplot([Size_G1S_CA_OE,Size_G1S_CA_WT,Size_G1S_CA_UE])
set(gca,'XTick',1:3, 'XTickLabel',{'CDKA:CYCD OE','CDKA:CYCD WT','CDKA:CYCD UE'})
xtickangle(gca,theta)
title('\textbf{A} - Size at G1/S','interpreter','latex')

subplot(v,h,2)
boxplot([Time_G1S_CA_OE,Time_G1S_CA_WT,Time_G1S_CA_UE],'Labels',{'CDKA:CYCD OE','CDKA:CYCD WT','CDKA:CYCD UE'})
set(gca,'XTick',1:3, 'XTickLabel',{'CDKA:CYCD OE','CDKA:CYCD WT','CDKA:CYCD UE'}) 
xtickangle(gca,theta)
title('\textbf{B} - G1 duration','interpreter','latex')

subplot(v,h,3)
boxplot([Size_Div_CA_OE,Size_Div_CA_WT,Size_Div_CA_UE],'Labels',{'CDKA:CYCD OE','CDKA:CYCD WT','CDKA:CYCD UE'})
set(gca,'XTick',1:3, 'XTickLabel',{'CDKA:CYCD OE','CDKA:CYCD WT','CDKA:CYCD UE'})
xtickangle(gca,theta)
title('\textbf{C} - Size at division','interpreter','latex')

subplot(v,h,4)
boxplot([Time_Div_CA_OE,Time_Div_CA_WT,Time_Div_CA_UE],'Labels',{'CDKA:CYCD OE','CDKA:CYCD WT','CDKA:CYCD UE'})
set(gca,'XTick',1:3, 'XTickLabel',{'CDKA:CYCD OE','CDKA:CYCD WT','CDKA:CYCD UE'})
xtickangle(gca,theta)
title('\textbf{D} - Cell cycle duration','interpreter','latex')
