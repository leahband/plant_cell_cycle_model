figure

G2MVolumeLoop(false,false)
G2MVolumeLoop(false,true)
G2MVolumeLoop(true,false)

subplot(2,1,1)
title('\textbf{A}','interpreter','latex')
xlabel('G1/S volume /Volume','interpreter','latex')
ylabel('G2 duration /Time','interpreter','latex')
legend('Hypothesis 0','Hypothesis 1 - Constant SMR','Hypothesis 2 - SMR SI','interpreter','latex','location','northeast')
grid minor

subplot(2,1,2)
title('\textbf{B}','interpreter','latex')
xlabel('G1/S volume /Volume','interpreter','latex')
ylabel('G2 added volume /Volume','interpreter','latex')
grid minor

%%
function G2MVolumeLoop(SMRSI,SMRConstant)

npts=100;
Vmin=0.1;
Vmax=1;
V0s=linspace(Vmin,Vmax,npts);
Ts=nan(1,npts);
VGs=nan(1,npts);

for k=1:npts
    V0=V0s(k);
    [TS,YS,DNF]=G2M_(V0,SMRSI,SMRConstant);
    VS=YS(:,1);
    Ts(k)=TS(end)-TS(1);
    VGs(k)=VS(end)-VS(1);
end

subplot(2,1,1)
hold on
p=plot(V0s,Ts,'linewidth',1.2);
xlim([Vmin Vmax])

subplot(2,1,2)
hold on
p=plot(V0s,VGs,'linewidth',1.2);
ylim([0 1])
xlim([Vmin Vmax])

end

%%
function [TS,YS,DNF]=G2M_(V0,SMRSI,SMRConstant);
Myb3At=0.6;

terminationTime=10999;

DNF=false;
hasReachedG1S=false;
hasReachedG2M=false;

rgr=6.6649e-04;

kskrp=0.01; 
kskrpa=.00;

kdcdka1=0.00; %0.06, 0.1
kdcdka2=.5;

ksSCF=0.01;
kdSCF=0.01;

ksE2FB=0.01;
kdE2FB=0.01;

%G2M parameters

kscdkb=0.01;
kdcdkb=0.01;
kdcdkb1=.1;

kpmyb4=2;
kdpmyb4=0.25; 

ksmyb4a=0.01;
ksmyb4=0.11;
kdmyb4a=0.1;

ksAPC=0.01;
kdAPC=0.01;

kscdka=0.01;
kdcdka=0.01;
kdkrpa=0.01; 
kdkrp=1;
RbT=2; 
kprb=1; 
kdprb=0.25;
E2FT=1; 
KdRE=0.001;
ks17a=0; 
ks17=0.1; 
kd17=0.1; 
kdAK=0.01;


if SMRConstant
    ssmr=0.00;
else
    ssmr=0.01;
end




dsmr=0.01;
dpsmr=0.1;

kd=0.001;
kpd=.25;

Myb3T=1;

%% ODE initialisation

CdkAT0=0;
KRPT0=0.9;
E2FB0=0;
SCF0=0;
CdkBT0=0;
SMRT0=1;
if SMRConstant
    SMRT0=SMRT0/V0;
end
Myb40=0;
APC0=0;

y0=[V0;CdkBT0;SMRT0];
%y0=[Myb3At    0.0881    0.4642    0.2718    0.4507    0.2891    0.2033    0.3662    0.2111]';

tspan=500;

options = odeset('Events',@EventsFcn);

tc=0;
yc=y0;

TS=tc;
YS=yc';

tsPhase=nan(4,1);
ysPhase=nan(4,length(yc));
Phase={'Birth','G1/S','G2/M','Cytokinesis'};

tsPhase(1)=tc;
ysPhase(1,:)=yc';

HasG1S=false;
HasG2M=false;
%% ODE

terminateCondition=false;
while ~terminateCondition
    [ts,ys]=ode45(@model,[tc tc+tspan],yc,options);
    tc=ts(end);
    yc=ys(end,:)';

    TS=[TS;ts(2:end)];
    YS=[YS;ys(2:end,:)];
    
    [Myb3A_]=Equilibria(tc,yc);
        
    G2M = (Myb3A_<Myb3At);

    if G2M&&(~HasG2M)
        HasG2M = true;
        tsPhase(3)=tc;
        ysPhase(3,:)=yc';
    end
   
    %figure; hold on; plot(SMRT_,zeros(size(SMRT_)),'k'); plot(SMRT_,DSMRT_);
    terminateCondition = G2M||(tc>=terminationTime);    
end

if tc>=terminationTime
    TS=nan;
    YS=nan;
    DNF=true;
end

%[E2FAs,Myb3As]=Equilibria2(TS,YS);
%size(E2FAs)
%plot(TS,Myb3As)

%% ODE functions

    function dydt=model(~,y)
        V=y(1);
        CdkBT=y(2);
        SMRT=y(3);

        %G2/M equilibrium
        CdkBSMR = compl(CdkBT,SMRT,kd);
        CdkB = CdkBT - CdkBSMR;
        pSMR = SMRT.*(CdkB./(CdkB + kpd));
        Myb3P = Myb3T.*(kpmyb4).*(CdkB)./((kpmyb4).*CdkB + kdpmyb4);
        
        %G2/M dynamics
        DV = rgr*V;
        DCdkBT = kscdkb - (kdcdkb)*CdkBT;
        
        if SMRSI
            DSMRT = ssmr/V - dsmr*(SMRT-pSMR) - dpsmr*pSMR; 
        else
            DSMRT = ssmr - dsmr*(SMRT-pSMR) - dpsmr*pSMR; 
        end
        
        dydt=[DV;DCdkBT;DSMRT];
    end

%% Events function

    function [position,isterminal,direction] = EventsFcn(t,y)
        [Myb3A]=Equilibria(t,y);   
        position = [Myb3A-Myb3At]; 
        isterminal = [1];  
        direction = [0];   
    end

    function [Myb3A]=Equilibria(t,y)
        V=y(1);
        CdkBT=y(2);
        SMRT=y(3);

        %G2/M equilibrium
        CdkBSMR = compl(CdkBT,SMRT,kd);
        CdkB = CdkBT - CdkBSMR;
        pSMR = SMRT.*(CdkB./(CdkB + kpd));
        Myb3P = Myb3T.*(kpmyb4).*(CdkB)./((kpmyb4).*CdkB + kdpmyb4);
        
        Myb3A=(Myb3T-Myb3P)/Myb3T;
    end

%% Function hell

    function out=Trace(arg1,arg2,arg3)
        out=arg1+arg2+arg3;
    end

    function out=compl(arg1,arg2,arg3)
        out=2.*arg1.*arg2./(Trace(arg1,arg2,arg3)+sqrt(Trace(arg1,arg2,arg3).^2-4.*arg1.*arg2));
        %out=(Trace(arg1,arg2,arg3)+sqrt(Trace(arg1,arg2,arg3).^2-4.*arg1.*arg2));
    end
end