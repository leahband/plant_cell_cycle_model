figure

g1SVolumeLoop(false,false,false)
g1SVolumeLoop(false,false,true)
g1SVolumeLoop(true,false,false)
g1SVolumeLoop(false,true,false)

subplot(2,1,1)
title('\textbf{A}','interpreter','latex')
xlabel('Birth volume /Volume','interpreter','latex')
ylabel('G1 duration /Time','interpreter','latex')
grid minor

subplot(2,1,2)
title('\textbf{B}','interpreter','latex')
xlabel('Birth volume /Volume','interpreter','latex')
ylabel('G1 added volume /Volume','interpreter','latex')
grid minor
%%
function g1SVolumeLoop(KRPSI,RBRSI,KRPconstant)

npts=100;
Vmin=0.1;
Vmax=1;
V0s=linspace(Vmin,Vmax,npts);
Ts=nan(1,npts);
VGs=nan(1,npts);

for k=1:npts
    V0=V0s(k);
    [TS,YS,DNF]=G1S_(V0,KRPSI,RBRSI,KRPconstant);
    VS=YS(:,1);
    Ts(k)=TS(end)-TS(1);
    VGs(k)=VS(end)-VS(1);
end

subplot(2,1,1)
hold on
p=plot(V0s,Ts,'linewidth',1.2);
xlim([Vmin Vmax])

subplot(2,1,2)
hold on
p=plot(V0s,VGs,'linewidth',1.2);
ylim([0 1])
xlim([Vmin Vmax])

end

%%
function [TS,YS,DNF]=G1S_(V0,KRPSI,RBRSI,KRPconstant)
terminationTime=10999;
E2FAt=0.2;

DNF=false;
hasReachedG1S=false;
hasReachedG2M=false;

rgr=6.6649e-04;

kskrpa=.00;

ksrb=0.02;
kdrb=0.01;

kdcdka1=0.00; %0.06, 0.1
kdcdka2=.5;

ksSCF=0.01;
kdSCF=0.01;

ksE2FB=0.01;
kdE2FB=0.01;

%G2M parameters

kscdkb=0.01/0.6;
kdcdkb=0.01;
kdcdkb1=.1;

kpmyb4=2;
kdpmyb4=E2FAt; 

ksmyb4a=0.01;
ksmyb4=0.11;
kdmyb4a=0.1;

ksAPC=0.01;
kdAPC=0.01;

kscdka=0.01;
kdcdka=0.01;

if KRPconstant
    kskrp=0;
else
    kskrp=0.01; 
end


kdkrpa=0.01; 

kdkrp=1;
kprb=1; 
kdprb=E2FAt;
E2FT=1; 
KdRE=0.001;
ks17a=0; 
ks17=0.1; 
kd17=0.1; 
kdAK=0.01;

ssmr=0.01;
dsmr=0.01;
dpsmr=0.1;

kd=0.001;
kpd=.25;

Myb3T=1;

%% ODE initialisation

CdkAT0=0;
KRPT0=0.9;
RBRT0=2;
E2FB0=0;
SCF0=0;
CdkBT0=0;
SMR0=1;
Myb40=0;
APC0=0;

%y0=[CdkAT0;KRPT0;E2FB0;SCF0;CdkBT0;SMR0;Myb40;APC0];

if KRPconstant
    KRPT0=KRPT0/V0;
end

y0=[V0    CdkAT0    KRPT0    RBRT0]';

tspan=500;

options = odeset('Events',@EventsFcn);

tc=0;
yc=y0;

TS=tc;
YS=yc';

tsPhase=nan(4,1);
ysPhase=nan(4,length(yc));
Phase={'Birth','G1/S','G2/M','Cytokinesis'};

tsPhase(1)=tc;
ysPhase(1,:)=yc';

HasG1S=false;
HasG2M=false;
%% ODE

terminateCondition=false;
while ~terminateCondition
    [ts,ys]=ode45(@model,[tc tc+tspan],yc,options);
    tc=ts(end);
    yc=ys(end,:)';

    TS=[TS;ts(2:end)];
    YS=[YS;ys(2:end,:)];
    
    [E2FA_]=Equilibria(tc,yc);
        
    G1S = (E2FA_>E2FAt);
 
    if G1S&&(~HasG1S)
        HasG1S = true;
    end

    %figure; hold on; plot(SMRT_,zeros(size(SMRT_)),'k'); plot(SMRT_,DSMRT_);
    terminateCondition = G1S||(tc>=terminationTime);    
end

if tc>=terminationTime
    %TS=nan;
    %YS=nan;
    DNF=true;
end

%[E2FAs,Myb3As]=Equilibria2(TS,YS);
%size(E2FAs)
%plot(TS,Myb3As)

%% ODE functions

    function dydt=model(~,y)
        V=y(1);
        CdkAT=y(2);
        KRPT=y(3);
        RbT=y(4);
        
        %G1/S Equilibrium
        CdkAKRP = compl(KRPT,CdkAT,kdAK);
        CdkA = CdkAT - CdkAKRP;
        RbP = RbT.*(kprb).*CdkA./((kprb).*CdkA + kdprb);
        Rb = RbT - RbP;
        RbE2F = compl(Rb,E2FT,KdRE);
        E2F = E2FT - RbE2F;
        FBL17 = (ks17a + ks17.*E2F)./kd17;
                        
        %G1/S dynamics
        DV = rgr*V;
        
        DCdkAT = kscdka - (kdcdka)*CdkAT;
        
        if KRPSI
            DKRPT = kskrp/V - (kdkrpa + kdkrp*FBL17)*KRPT;
        else
            DKRPT = kskrp - (kdkrpa + kdkrp*FBL17)*KRPT;
        end
 
        if RBRSI
            DRBT = ksrb/V - (kdrb)*RbT;
        else
            DRBT = ksrb - (kdrb)*RbT;
        end
 
        dydt=[DV;DCdkAT;DKRPT;DRBT];
    end

%% Events function

    function [position,isterminal,direction] = EventsFcn(t,y)
        [E2FA]=Equilibria(t,y);   
        position = [E2FA-E2FAt]; 
        isterminal = [1];  
        direction = [0];   
    end

    function [E2FA,Myb3A]=Equilibria(t,y)
        V=y(1);
        CdkAT=y(2);
        KRPT=y(3);
        RbT=y(4);
        
        %G1/S Equilibrium
        CdkAKRP = compl(KRPT,CdkAT,kdAK);
        CdkA = CdkAT - CdkAKRP;
        RbP = RbT.*(kprb).*CdkA./((kprb).*CdkA + kdprb);
        Rb = RbT - RbP;
        RbE2F = compl(Rb,E2FT,KdRE);
        E2F = E2FT - RbE2F;
        FBL17 = (ks17a + ks17.*E2F)./kd17;

        E2FA=E2F/E2FT;
    end

%% Function hell

    function out=Trace(arg1,arg2,arg3)
        out=arg1+arg2+arg3;
    end

    function out=compl(arg1,arg2,arg3)
        out=2.*arg1.*arg2./(Trace(arg1,arg2,arg3)+sqrt(Trace(arg1,arg2,arg3).^2-4.*arg1.*arg2));
        %out=(Trace(arg1,arg2,arg3)+sqrt(Trace(arg1,arg2,arg3).^2-4.*arg1.*arg2));
    end
end