figure
%%
CellData=MultiCellModel_Sizer_Adder_Timer('sizer');

ax=subplot(3,2,1);
TimeCourse(CellData,ax)
title(ax,'\textbf{A} - sizer','interpreter','latex')

ax=subplot(3,2,2);
ScatterPlot(CellData,ax)
title(ax,'\textbf{B} - sizer','interpreter','latex')
%%
CellData=MultiCellModel_Sizer_Adder_Timer('adder');

ax=subplot(3,2,3);
TimeCourse(CellData,ax)
title(ax,'\textbf{C} - adder','interpreter','latex')

ax=subplot(3,2,4);
ScatterPlot(CellData,ax)
ylim(ax,[0.8 1.2])
title(ax,'\textbf{D} - adder','interpreter','latex')
%%
CellData=MultiCellModel_Sizer_Adder_Timer('timer');

ax=subplot(3,2,5);
TimeCourse(CellData,ax)
title(ax,'\textbf{E} - timer','interpreter','latex')

ax=subplot(3,2,6);
ScatterPlot(CellData,ax)
title(ax,'\textbf{F} - timer','interpreter','latex')
%%
function ScatterPlot(CellData,ax)
    hold(ax,'on')
    t1=1;
    t2=2;

    nCells=numel(CellData);

    BirthSizes=nan(nCells,1);
    DivisionSizes=nan(nCells,1);
    tCellCycle=nan(nCells,1);

    for cellNo=1:nCells
        ysPhase = CellData(cellNo).ysPhase;
        tsPhase = CellData(cellNo).tsPhase;
        VsPhase = ysPhase(:,1);
        BirthSizes(cellNo)=VsPhase(t1);
        DivisionSizes(cellNo)=VsPhase(t2);
        tCellCycle(cellNo)=tsPhase(t2)-tsPhase(t1);
    end

    %plot(BirthSizes,DivisionSizes-BirthSizes,'x')
    X=BirthSizes;

    Y=DivisionSizes-BirthSizes;
    %Y=tCellCycle;

    c=lines;

    p1=plot(ax,X(2:end),Y(2:end),'x','color',c(1,:));
    xlabel(ax,'Birth volume /Volume','interpreter','latex')
    ylabel(ax,'Added volume /Volume','interpreter','latex')
    grid(ax,'minor')
    
end
%%
function TimeCourse(CellData,ax)

    dt=1e-3;

    cellNos=numel(CellData);

    hold on

    c=lines;

    for cellNo=1:cellNos

        ts=[]; ys=[];
        TS0=[];
        TS=[];
        YS={};
        while cellNo>0

            ts=CellData(cellNo).TS; 
            ys=CellData(cellNo).YS; 


            TS0=[TS0;ts(1)];
            TS=[TS,{ts}];
            YS=[YS,{ys}];

            cellNo=CellData(cellNo).parent;
        end

        [TS0s,Is]=sort(TS0,'ascend');
        TSs=TS(Is);
        YSs=YS(Is);

        nCL=numel(TSs);

        ts=[];
        Ts=[];
        Ys=[];
        for k=1:nCL
           tsNew=TSs{k}; 

           if ~isempty(ts)
               tsNew(1)=ts(end)+dt;
           end
           ts=tsNew;

           ys=YSs{k};

           Ts=[Ts;ts];
           Ys=[Ys;ys];

        end

        plot(ax,Ts,Ys(:,1),'color',c(1,:))
    end
    xlabel(ax,'Time','interpreter','latex')
    ylabel(ax,'Volume','interpreter','latex')
end

%%
function CellData=MultiCellModel_Sizer_Adder_Timer(celltype);

%probability distribution
mu=0.5;
sigma=0.05;
norm = makedist('normal');
norm.mu=mu;
norm.sigma=sigma;
normt=truncate(norm,0,1);

%--------------------------------------------------------------------------

doLoop=true;

nCells=64;
t0=0;
Y0=1;

cellsDone=0;
cellNo=1;

t0s={t0};
Y0s={Y0}; 
parents={0};

CellData=struct;
CellData.TS=[]; CellData.YS=[]; CellData.tsPhase=[]; CellData.ysPhase=[]; CellData.parent=[];

    while doLoop
        
        t0=t0s{1}; Y0=Y0s{1}; parent=parents{1};
        
        [TS,YS,tsPhase,ysPhase,DNF]=SingleCellCycle_Sizer_Adder_Timer(t0,Y0,celltype);
        
        if DNF
            break
        else
            %record data
            CellData(cellNo).TS=TS; 
            CellData(cellNo).YS=YS; 
            CellData(cellNo).tsPhase=tsPhase; 
            CellData(cellNo).ysPhase=ysPhase;
            CellData(cellNo).parent=parent;
            
            yEnd=YS(end,:);
            tEnd=TS(end);
            
            VEnd=yEnd(1);
            
            
            [Y1,Y2]=CellDivision(yEnd);
            
            t0s=[t0s,{tEnd},{tEnd}];
            Y0s=[Y0s,{Y1},{Y2}];
            parents=[parents,{cellNo},{cellNo}];
            
            t0s=t0s(2:end); %remove the cell we've just done
            Y0s=Y0s(2:end);
            parents=parents(2:end);
            
            cellNo=cellNo+1;
            cellsDone=cellsDone+1;
            
            
        end
        
        doLoop=cellsDone<nCells;
        
    end

    function [y1,y2]=CellDivision(y)
                
        %distribute volume randomly
        d=random(normt);
        
        %volume
        V0=y;
        V1=d*V0;
        V2=V0-V1;
        y1(1)=V1;
        y2(1)=V2;

    end
    
    
end

%%
function [TS,YS,tsPhase,ysPhase,DNF]=SingleCellCycle_Sizer_Adder_Timer(t0,y0,celltype)

if isrow(y0)
    y0=y0';
end

terminationTime=10999;

DNF=false;

rgr=6.6649e-04; %relative growth rate

V0=y0(1);
Vts=2; %threshold division size for sizer. Default: 2.
Vta = V0+1; %threshold added volume for adder. Default: 1
tt = t0+ log(2)/rgr; %Time until division. Default: log(2)/rgr
%% ODE initialisation

tspan=500;

options = odeset('Events',@EventsFcn);

tc=t0;
yc=y0;

TS=tc;
YS=yc';

tsPhase=nan(2,1);
ysPhase=nan(2,length(yc));
Phase={'Birth','Cytokinesis'};

tsPhase(1)=tc;
ysPhase(1,:)=yc';

%% ODE

terminateCondition=false;
while ~terminateCondition
    [ts,ys]=ode45(@model,[tc tc+tspan],yc,options);
    tc=ts(end);
    yc=ys(end,:)';

    TS=[TS;ts(2:end)];
    YS=[YS;ys(2:end,:)];
    
    V_=YS(end);
    t_=TS(end);
    
    if strcmpi(celltype,'sizer')
        Cytokinesis=V_>=Vts; 
    elseif strcmpi(celltype,'adder')
        Cytokinesis=V_>=Vta; 
    elseif strcmpi(celltype,'timer')
        Cytokinesis=t_>=tt; 
    end

    if Cytokinesis
        tsPhase(2)=tc;
        ysPhase(2,:)=yc';
    end
    
    %figure; hold on; plot(SMRT_,zeros(size(SMRT_)),'k'); plot(SMRT_,DSMRT_);
    terminateCondition = Cytokinesis||(tc>=terminationTime);    
end

if tc>=terminationTime
    TS=nan;
    YS=nan;
    DNF=true;
end

%% ODE functions

    function dydt=model(~,y)
        V=y(1); 
        DV = rgr*V;
        dydt=DV;
    end

%% Events function

    function [position,isterminal,direction] = EventsFcn(t,y)
        V=y(1);
        if strcmpi(celltype,'sizer')
            position=V-Vts; 
        elseif strcmpi(celltype,'adder')
            position=V-Vta; 
        elseif strcmpi(celltype,'timer')
            position=t-tt; 
        end
        
        isterminal = 1;  
        direction = 0;   
    end
 
%%

    function out=Trace(arg1,arg2,arg3)
        out=arg1+arg2+arg3;
    end

    function out=compl(arg1,arg2,arg3)
        out=2.*arg1.*arg2./(Trace(arg1,arg2,arg3)+sqrt(Trace(arg1,arg2,arg3).^2-4.*arg1.*arg2));
        %out=(Trace(arg1,arg2,arg3)+sqrt(Trace(arg1,arg2,arg3).^2-4.*arg1.*arg2));
    end
end