G1S
G1SBifurcationLoop
%%
function G1S;

kskrp=0.01; 
kdkrpa=0.01; 
kdkrp=1;
kscdka=0.01;
kdcdka=0.01;
RbT=2; 
kprb=1; 
kdprb=0.25;
E2FT=1; 
KdRE=0.001;
ks17a=0; 
ks17=0.1; 
kd17=0.1; 
kdAK=0.01;

CdkAT0=0;
KRPT0=1;

y0=[CdkAT0;KRPT0];
tspan=1000;

[ts,ys]=ode45(@model,[0 tspan],y0);
plot(ts,ys)
CdkATs=ys(:,1);
KRPTs=ys(:,2);
[E2Fs,Rbs]=model2(CdkATs,KRPTs);

subplot(2,1,1)
hold on
plot(ts,CdkATs,'linewidth',1.2);
plot(ts,KRPTs,'linewidth',1.2);
plot(ts,Rbs,'linewidth',1.2);
plot(ts,E2Fs,'linewidth',1.2);
legend('CDKA:CYCD','KRP','uRBR','E2FA','location','northeast','interpreter','latex')
xlabel('Time since birth /Units','interpreter','latex')
ylabel('Protein concentration','interpreter','latex')
title('\textbf{A}','interpreter','latex')


    function dydt=model(t,y)
        CdkAT=y(1);
        KRPT=y(2);
        
        CdkAKRP = compl(KRPT,CdkAT,kdAK);
        CdkA = CdkAT - CdkAKRP;
        RbP = RbT*(kprb)*CdkA/((kprb)*CdkA + kdprb);
        Rb = RbT - RbP;
        RbE2F = compl(Rb,E2FT,KdRE);
        E2F = E2FT - RbE2F;
        FBL17 = (ks17a + ks17*E2F)/kd17;
        
        DCdkAT = kscdka - kdcdka*CdkAT;
        DKRPT = kskrp - (kdkrpa + kdkrp*FBL17)*KRPT;
        
        dydt=[DCdkAT;DKRPT];
    end


    function [E2F,Rb]=model2(CdkAT,KRPT)

        CdkAKRP = compl(KRPT,CdkAT,kdAK);
        CdkA = CdkAT - CdkAKRP;
        RbP = RbT.*(kprb).*CdkA./((kprb).*CdkA + kdprb);
        Rb = RbT - RbP;
        RbE2F = compl(Rb,E2FT,KdRE);
        E2F = E2FT - RbE2F;
        FBL17 = (ks17a + ks17.*E2F)./kd17;
        
    end

    function out=Trace(arg1,arg2,arg3)
        out=arg1+arg2+arg3;
    end

    function out=compl(arg1,arg2,arg3)
        out=2.*arg1.*arg2./(Trace(arg1,arg2,arg3)+sqrt(Trace(arg1,arg2,arg3).^2-4.*arg1.*arg2));
        %out=(Trace(arg1,arg2,arg3)+sqrt(Trace(arg1,arg2,arg3)^2-4.*arg1.*arg2));
    end
    
end

%%
function G1SBifurcationLoop
n=10000;
CdkATs=linspace(0,1,n);

maxlengths=zeros(1,n);
for k=1:n
    [StdSts,Stab]=G1SBifurcation(CdkATs(k));
    maxlengths(k)=numel(StdSts);
end

maxlength=max(maxlengths);

StdSts_=nan(maxlength,n);
Stab_=nan(maxlength,n);

for k=1:n
    [StdSts,Stab]=G1SBifurcation(CdkATs(k));
    m=length(StdSts);
    if m>=1
        StdSts_(1:m,k)=StdSts';
        Stab_(1:m,k)=Stab;
    end
end

CdkATs_=repmat(CdkATs,maxlength,1);

idx=~isnan(StdSts_);
StdSts_=StdSts_(idx);
Stab_=Stab_(idx);
CdkATs_=CdkATs_(idx);

[StdSts_,idx]=sort(StdSts_,'ascend');
CdkATs_=CdkATs_(idx);
Stab_=Stab_(idx);

idx=Stab_>0;
StdSts_u=StdSts_(idx);
CdkATs_u=CdkATs_(idx);

subplot(2,1,2)
plot(CdkATs_,StdSts_,'linewidth',1.2)
hold on
plot(CdkATs_u,StdSts_u,'linewidth',1.2)
legend('Stable','Unstable','interpreter','latex','location','southwest')

xlabel('CDKA:CYCD','interpreter','latex')
ylabel('KRP','interpreter','latex')
title('\textbf{B}','interpreter','latex')
end

%%
function [StdSts,Stab]=G1SBifurcation(CdkAT);

kskrp=0.01; 
kdkrpa=0.01; 
kdkrp=1;
RbT=2; 
kprb=1; 
kdprb=0.25;
E2FT=1; 
KdRE=0.001;
ks17a=0; 
ks17=0.1; 
kd17=0.1; 
kdAK=0.01;

npts=1000;
KRPT=linspace(0,1,npts);

%
CdkAKRP = compl(KRPT,CdkAT,kdAK);
CdkA = CdkAT - CdkAKRP;
RbP = RbT.*(kprb).*CdkA./((kprb).*CdkA + kdprb);
Rb = RbT - RbP;
RbE2F = compl(Rb,E2FT,KdRE);
E2F = E2FT - RbE2F;
FBL17 = (ks17a + ks17.*E2F)./kd17;
DKRPT = kskrp - (kdkrpa + kdkrp.*FBL17).*KRPT;
%
crsng=find(sign(DKRPT(2:end))~=sign(DKRPT(1:end-1)));

StdSts=[];
Stab=[];
if ~isempty(crsng)
    N=length(crsng);
    StdSts=nan(N,1);
    Stab=nan(N,1);
    for k=1:N
        crs=crsng(k);
        x1=KRPT(crs); x2=KRPT(crs+1);
        y1=DKRPT(crs); y2=DKRPT(crs+1);
        m=(y2-y1)/(x2-x1);
        c=y1-m*x1;
        KRPT0=-c/m;
        StdSts(k)=KRPT0;
        Stab(k)=sign(m);
    end
end

    function out=Trace(arg1,arg2,arg3)
        out=arg1+arg2+arg3;
    end

    function out=compl(arg1,arg2,arg3)
        out=2.*arg1.*arg2./(Trace(arg1,arg2,arg3)+sqrt(Trace(arg1,arg2,arg3).^2-4.*arg1.*arg2));
        %out=(Trace(arg1,arg2,arg3)+sqrt(Trace(arg1,arg2,arg3).^2-4.*arg1.*arg2));
    end
    
end
