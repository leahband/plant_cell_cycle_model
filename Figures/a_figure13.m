figure;
load('mutantdata.mat')

subplot(v,h,1)
boxplot([Size_G1S_CB_OE,Size_G1S_CB_WT,Size_G1S_CB_UE])
set(gca,'XTick',1:3, 'XTickLabel',{'CDKB:CYCB OE','CDKB:CYCB WT','CDKB:CYCB UE'})
xtickangle(gca,theta)
title('\textbf{A} - Size at G1/S','interpreter','latex')

subplot(v,h,2)
boxplot([Time_G1S_CB_OE,Time_G1S_CB_WT,Time_G1S_CB_UE],'Labels',{'CDKB:CYCB OE','CDKB:CYCB WT','CDKB:CYCB UE'})
set(gca,'XTick',1:3, 'XTickLabel',{'CDKB:CYCB OE','CDKB:CYCB WT','CDKB:CYCB UE'})
xtickangle(gca,theta)
title('\textbf{B} - G1 duration','interpreter','latex')

subplot(v,h,3)
boxplot([Size_Div_CB_OE,Size_Div_CB_WT,Size_Div_CB_UE],'Labels',{'CDKB:CYCB OE','CDKB:CYCB WT','CDKB:CYCB UE'})
set(gca,'XTick',1:3, 'XTickLabel',{'CDKB:CYCB OE','CDKB:CYCB WT','CDKB:CYCB UE'})
xtickangle(gca,theta)
title('\textbf{C} - Size at division','interpreter','latex')

subplot(v,h,4)
boxplot([Time_Div_CB_OE,Time_Div_CB_WT,Time_Div_CB_UE],'Labels',{'CDKB:CYCB OE','CDKB:CYCB WT','CDKB:CYCB UE'})
set(gca,'XTick',1:3, 'XTickLabel',{'CDKB:CYCB OE','CDKB:CYCB WT','CDKB:CYCB UE'})
xtickangle(gca,theta)
title('\textbf{D} - Cell cycle duration','interpreter','latex')

set(gcf, 'Color', 'w');