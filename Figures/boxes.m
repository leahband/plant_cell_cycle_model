up=0.01;
%%
hold on

c=lines;
c1=c(1,:);
c11=[166,206,232]/255;
c22=[242 195 175]/255;

%y1=-0.1;
%y2=0.0;

g1=G1(1,:);

pgon=polyshape([g1(1) g1(2) g1(2) g1(1)],[y1 y1 y2 y2]);
p11=plot(pgon,'FaceColor',c11,'FaceAlpha',1.0);
text(g1(1)+(g1(2)-g1(1))*0.15,mean([y1 y2]) + up,'G1','FontSize',6);

g1=G1(2,:);

pgon=polyshape([g1(1) g1(2) g1(2) g1(1)],[y1 y1 y2 y2]);
plot(pgon,'FaceColor',c11,'FaceAlpha',1.0);
text(g1(1)+(g1(2)-g1(1))*0.15,mean([y1 y2]) + up,'G1','FontSize',6);

g1=G1(3,:);

pgon=polyshape([g1(1) g1(2) g1(2) g1(1)],[y1 y1 y2 y2]);
plot(pgon,'FaceColor',c11,'FaceAlpha',1.0);
text(g1(1)+(g1(2)-g1(1))*0.15,mean([y1 y2]) + up,'G1','FontSize',6);

g1=G1(4,:);

pgon=polyshape([g1(1) g1(2) g1(2) g1(1)],[y1 y1 y2 y2]);
plot(pgon,'FaceColor',c11,'FaceAlpha',1.0);
text(g1(1)+(g1(2)-g1(1))*0.15,mean([y1 y2]) + up,'G1','FontSize',6);

%%

%y1=-0.1;
%y2=0.0;

g1=G2(1,:);

pgon=polyshape([g1(1) g1(2) g1(2) g1(1)],[y1 y1 y2 y2]);
hold on
p22=plot(pgon,'FaceColor',c22,'FaceAlpha',1.0);
text(g1(1)+(g1(2)-g1(1))*0.1,mean([y1 y2]) + up,'G2','FontSize',6);

g1=G2(2,:);

pgon=polyshape([g1(1) g1(2) g1(2) g1(1)],[y1 y1 y2 y2]);
hold on
plot(pgon,'FaceColor',c22,'FaceAlpha',1.0)
text(g1(1)+(g1(2)-g1(1))*0.15,mean([y1 y2]) + up,'G2','FontSize',6);

g1=G2(3,:);

pgon=polyshape([g1(1) g1(2) g1(2) g1(1)],[y1 y1 y2 y2]);
hold on
plot(pgon,'FaceColor',c22,'FaceAlpha',1.0)
text(g1(1)+(g1(2)-g1(1))*0.15,mean([y1 y2]) + up,'G2','FontSize',6);

g1=G2(4,:);

pgon=polyshape([g1(1) g1(2) g1(2) g1(1)],[y1 y1 y2 y2]);
hold on
plot(pgon,'FaceColor',c22,'FaceAlpha',1.0)
text(g1(1)+(g1(2)-g1(1))*0.15,mean([y1 y2]) + up,'G2','FontSize',6);