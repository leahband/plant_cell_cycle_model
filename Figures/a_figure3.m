TS=[]; YS=[];

nCycles=4;
y0=[0.0881    0.4642    0.2718    0.4507    0.2891    0.2033    0.3662    0.2111]';
t0=0;

G1 = zeros(nCycles,2);
G2 = zeros(nCycles,2);


for k=1:nCycles
    [ts,ys,tsPhase,ysPhase,DNF]=SingleCellCycle(t0,y0);
    t0=ts(end);
    y0=ys(end,:);
    TS=[TS;ts(2:end)];
    YS=[YS;ys(2:end,:)];
    G1(k,:)=tsPhase(1:2)';
    G2(k,:)=tsPhase([2,4])';
end

CdkAT=YS(:,1);
CdkBT=YS(:,5);
KRPT=YS(:,2);
SMRT=YS(:,6);
E2FB=YS(:,3);
Myb4T=YS(:,7);
SCF=YS(:,4);
APC=YS(:,8);

y1=-0.1;
y2=0.0;


%parameters

kskrp=0.01; 
kskrpa=.00;

kdcdka1=0.00; %0.06, 0.1
kdcdka2=.5;

ksSCF=0.01;
kdSCF=0.01;

ksE2FB=0.01;
kdE2FB=0.01;

%G2M parameters

kscdkb=0.01/0.6;
kdcdkb=0.01;
kdcdkb1=.1;

kpmyb4=2;
kdpmyb4=0.25; 

ksmyb4a=0.01;
ksmyb4=0.11;
kdmyb4a=0.1;

ksAPC=0.01;
kdAPC=0.01;

kscdka=0.01;
kdcdka=0.01;
kdkrpa=0.01; 
kdkrp=1;
RbT=2; 
kprb=1; 
kdprb=0.25;
E2FT=1; 
KdRE=0.001;
ks17a=0; 
ks17=0.1; 
kd17=0.1; 
kdAK=0.01;

ssmr=0.01;
dsmr=0.01;
dpsmr=0.1;

kd=0.001;
kpd=.25;

Myb3T=1;


%G1/S Equilibrium
CdkAKRP = compl(KRPT,CdkAT,kdAK);
CdkA = CdkAT - CdkAKRP;
RbP = RbT.*(kprb).*CdkA./((kprb).*CdkA + kdprb);
Rb = RbT - RbP;
RbE2F = compl(Rb,E2FT,KdRE);
E2F = E2FT - RbE2F;
FBL17 = (ks17a + ks17.*E2F)./kd17;

%G2/M equilibrium
CdkBSMR = compl(CdkBT,SMRT,kd);
CdkB = CdkBT - CdkBSMR;
pSMR = SMRT.*(CdkB./(CdkB + kpd));
Myb4P = Myb4T.*(kpmyb4).*(CdkB)./((kpmyb4).*CdkB + kdpmyb4);
Myb3P = Myb3T.*(kpmyb4).*(CdkB)./((kpmyb4).*CdkB + kdpmyb4);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

subplot(4,2,3)
hold on
boxes
p1=plot(TS,CdkAT,'linewidth',1.2);
p2=plot(TS,CdkBT,'linewidth',1.2);
ylim([-0.1 1])
legend([p1,p2],{'CDKA:CYCD','CDKB:CYCB'},'interpreter','latex','location','northeast')
xlabel('Time','interpreter','latex')
ylabel('Concentration','interpreter','latex')
title('\textbf{C} - time course CDK:Cyclins','interpreter','latex')

subplot(4,2,4)
hold on
boxes
p1=plot(TS,KRPT,'linewidth',1.2);
p2=plot(TS,SMRT,'linewidth',1.2);
ylim([-0.1 1])
legend([p1,p2],{'KRP','SMR'},'interpreter','latex','location','northeast')
xlabel('Time','interpreter','latex')
ylabel('Concentration','interpreter','latex')
title('\textbf{D} - time course CDK inhibitors','interpreter','latex')

y1=-0.06;
y2=0.0;

subplot(4,2,5)
hold on
boxes
p1=plot(TS,E2F,'linewidth',1.2);
p2=plot(TS,E2FB,'linewidth',1.2);
ylim([-0.06 .6])
legend([p1,p2],{'E2FA','E2FB'},'interpreter','latex','location','northeast')
xlabel('Time','interpreter','latex')
ylabel('Concentration','interpreter','latex')
title('\textbf{E} - time course E2F transcription factors','interpreter','latex')

subplot(4,2,6)
hold on
boxes
p1=plot(TS,SCF,'linewidth',1.2);
p2=plot(TS,APC,'linewidth',1.2);
p3=plot(TS,FBL17,'linewidth',1.2);
ylim([-0.06 .6])
legend([p1,p2,p3],{'SCF','APC','FBL17'},'interpreter','latex','location','northeast')
xlabel('Time','interpreter','latex')
ylabel('Concentration','interpreter','latex')
title('\textbf{F} - time course ligases','interpreter','latex')

y1=-0.1;
y2=0.0;
subplot(4,2,7)
hold on
boxes
p1=plot(TS,Myb3T-Myb3P,'linewidth',1.2);
p2=plot(TS,Myb4T,'linewidth',1.2);
ylim([-0.1 1])
legend([p1,p2],{'MYB3R3','MYB3R4'},'interpreter','latex','location','northeast')
xlabel('Time','interpreter','latex')
ylabel('Concentration','interpreter','latex')
title('\textbf{G} - time course MYB transcription factors','interpreter','latex')


y1=-0.2;
y2=0.0;
subplot(4,2,8)
hold on
boxes
p1=plot(TS,Rb,'linewidth',1.2);
p2=plot(TS,RbT*ones(size(TS)),'linewidth',1.2);
ylim([-0.2 2])
legend([p1,p2],{'uRBR','${RBR}_{T}$'},'interpreter','latex','location','northeast')
xlabel('Time','interpreter','latex')
ylabel('Concentration','interpreter','latex')
title('\textbf{H} - RBR','interpreter','latex')

subplot(4,2,3)
xlim([0 4369])
subplot(4,2,4)
xlim([0 4369])
subplot(4,2,5)
xlim([0 4369])
subplot(4,2,6)
xlim([0 4369])
subplot(4,2,7)
xlim([0 4369])
subplot(4,2,8)
xlim([0 4369])


%%

TS=[]; YS=[];

nCycles=4;
y0=[0.0881    0.4642    0.2718    0.4507    0.2891    0.2033    0.3662    0.2111]';
t0=0;


[ts,ys,tsPhase,ysPhase,DNF]=SingleCellCycle(t0,y0);
t0=ts(end);
y0=ys(end,:);
[TS,YS,tsPhase,ysPhase,DNF]=SingleCellCycle(t0,y0);

CdkAT=YS(:,1);
CdkBT=YS(:,5);
KRPT=YS(:,2);
SMRT=YS(:,6);
E2FB=YS(:,3);
Myb4T=YS(:,7);
SCF=YS(:,4);
APC=YS(:,8);

c=lines;

subplot(4,2,1)
cla

plot(CdkAT,KRPT,'linewidth',1.2);
hold on

N=length(CdkAT);
f1=0.2;
p1=plot(CdkAT(1:floor(N*f1)),KRPT(1:floor(N*f1)),'linewidth',1.2,'color',c(1,:));

%line2arrow(p1)
xlabel('CDKA:CYCD Concentration','interpreter','latex')
ylabel('KRP Concentration','interpreter','latex')
title('\textbf{A} - phase plane','interpreter','latex')

subplot(4,2,2)
plot(CdkBT,SMRT,'linewidth',1.2);
hold on

N=length(CdkBT);
f2=0.04;
p2=plot(CdkBT(1:floor(N*f2)),SMRT(1:floor(N*f2)),'linewidth',1.2,'color',c(1,:));

xlabel('CDKB:CYCB Concentration','interpreter','latex')
ylabel('SMR Concentration','interpreter','latex')
title('\textbf{B} - phase plane','interpreter','latex')

%%
function out=compl(arg1,arg2,arg3)
    out=2.*arg1.*arg2./(Trace(arg1,arg2,arg3)+sqrt(Trace(arg1,arg2,arg3).^2-4.*arg1.*arg2));
    %out=(Trace(arg1,arg2,arg3)+sqrt(Trace(arg1,arg2,arg3).^2-4.*arg1.*arg2));
end
function out=Trace(arg1,arg2,arg3)
    out=arg1+arg2+arg3;
end

