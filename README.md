# plant_cell_cycle_model

Code for paper:

Williamson D, Tasker-Brown W, Murray JAH, Jones AR and Band LR \
Modelling how plant cell-cycle progression leads to cell size
regulation \
PLoS Comput Biol \
2023

## Repository Structure:
'Figures' folder contains matlab code labelled by Figure number that produces each figures in the paper. 
This code calls the model codes in the folder 'Library'

## Authors
Code was written by lead author, Dr Daniel Williamson. 


